namespace Entities.Classes
{
   class ImportedProduct : Product
   {
      double CustomFees {get; set;}

      public ImportedProduct(string name, double price, double fees) : base(name, price)
      {
         this.CustomFees = fees;
      }

      public override string PriceTag()
      {
         return $"{this.Name} ${this.TotalPrice()} (Custom fees: ${this.CustomFees})";
      }

      public double TotalPrice()
      {
         return this.Price + this.CustomFees;
      }
   }
}