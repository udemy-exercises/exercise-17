using System;

namespace Entities.Classes
{
   class UsedProduct : Product
   {
      public DateOnly ManufactureDate {get; set;}

      public UsedProduct(string name, double price, string date) : base(name, price)
      {
         this.ManufactureDate = DateOnly.Parse(date);
      }

      public override string PriceTag()
      {
         return $"{this.Name} (used) ${this.Price} (Manufacture date: {this.ManufactureDate})";
      }
   }
}