﻿using System;
using System.Collections.Generic;
using Entities.Classes;

class Program
{
   static void Main()
   {
      Console.Write("Enter the number of products: ");
      int amountProducts = int.Parse(Console.ReadLine());

      List<Product> listProducts = new List<Product>();
      for (int i = 1; i < amountProducts +1; i++)
      {
         Console.WriteLine($"Product #{i} data");

         Console.Write("Common, used or imported? (c/u/i) ");
         string answer = Console.ReadLine();
         answer = answer.ToLower();
         if (answer != "c" && answer != "u" && answer != "i") {
            Console.WriteLine("ERROR: ANSWER NOT VALID"); 
            return;
         }

         Console.Write("Name: ");
         string name = Console.ReadLine();
         Console.Write("Price: ");
         double price = double.Parse(Console.ReadLine());

         if (answer == "c") {
            var product = new Product(name, price);
            listProducts.Add(product);
         }

         else if (answer == "i") {
            Console.Write("Custom fee: ");
            double customFee = double.Parse(Console.ReadLine());

            var importedProduct = new ImportedProduct(name, price, customFee);
            listProducts.Add(importedProduct);
         }

         else if (answer == "u") {
            Console.Write("Manufacture date (DD/MM/YYYY): ");
            string date = Console.ReadLine();

            var usedProduct = new UsedProduct(name, price, date);
            listProducts.Add(usedProduct);
         }
      }

      Console.WriteLine();
      Console.WriteLine("PRICE TAGS:");
      for (int i = 0; i < listProducts.Count; i++)
      {
         Console.WriteLine(listProducts[i].PriceTag());
      }
   }
}